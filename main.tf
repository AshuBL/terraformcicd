provider "aws" {
    region = var.region 
    access_key = var.access_key
    secret_key = var.secret_key  
}

# Create a Virtual Private Cloud (VPC)
resource "aws_vpc" "vpc" {
     cidr_block = "10.0.0.0/16" 
     tags = {
         Name = "AWS-Demo"
     }
     
}

# Create a  Internet Gateway
resource "aws_internet_gateway" "igw" {

     vpc_id = aws_vpc.vpc.id #refrencing vpc id 
     
}

# Create a Route Table 
resource "aws_route_table" "route-table" { 
     vpc_id = aws_vpc.vpc.id 
        route  {
                 cidr_block = "0.0.0.0/0" 
                 gateway_id = aws_internet_gateway.igw.id 
            }
         route             {
                 ipv6_cidr_block = "::/0"
                 gateway_id = aws_internet_gateway.igw.id 
            }
     tags = { 
         Name = "AWS-Demo-RT"
          } 
}

# Create a subnet on which our Windows server  
resource "aws_subnet" "subnet" {
    vpc_id = aws_vpc.vpc.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "us-east-2a"
    tags = {
        Name = "AWS-Demo-subnet"
    }
  
}

# Associate subnet with route table
resource "aws_route_table_association" "connection-RT-subnet" { 
    subnet_id = aws_subnet.subnet.id 
    route_table_id = aws_route_table.route-table.id 
 }

# Add Security Group, using the resource aws_security_group
resource "aws_security_group" "sg" {
    name = "security_group" 
    description = "Allow Web inbound traffic" 
    vpc_id = aws_vpc.vpc.id 
        ingress {
            description = "HTTPS" 
            from_port = 443
            to_port = 443 
            protocol = "tcp" 
            cidr_blocks = ["0.0.0.0/0"]
        } 

        ingress { 
            description = "HTTP"
            from_port = 80 
            to_port = 80 
            protocol = "tcp" 
            cidr_blocks = ["0.0.0.0/0"] 
         } 

        ingress {
            description = "SSH"
            from_port = 22 
            to_port = 22 
            protocol = "tcp" 
            cidr_blocks = ["0.0.0.0/0"]
        }

        egress {
            from_port = 0
            to_port = 0
            protocol = "-1" 
            cidr_blocks = ["0.0.0.0/0"] 
        } 
            tags = { 
                Name = " security_group"
            }
}

# Create a network interface with an ip in the subnet
resource "aws_network_interface" "server-nic" { 
    subnet_id = aws_subnet.subnet.id 
    private_ips = ["10.0.1.10"] 
    security_groups = [aws_security_group.sg.id]     
}

# Assign an elastic IP to the network interface
#resource "aws_eip" "one" { 
   # vpc = true 
   # network_interface = aws_network_interface.server-nic.id 
   # associate_with_private_ip = "10.0.1.10" 
    #depends_on = [aws_internet_gateway.igw]
#}

resource "aws_instance" "server-instance" {
     ami = var.ami
     #region = var.region
     instance_type = var.instance_type
     availability_zone = "us-east-2a"
     key_name = "RHEL" 
        
     network_interface { 
        device_index = 0 
        network_interface_id = aws_network_interface.server-nic.id
     } 

    user_data = <<-EOF
         #!/bin/bash 
         sudo apt update -y 
         sudo apt install apache2 -y
         sudo systemctl start apache2 
         sudo bash -c 'echo my very first web server > /var/www/html/index.html'
         EOF
tags = {
    Name = "AWS-Gitlab-Windows-Server"
}
}
