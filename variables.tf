# Creating region variable
variable "region" {
        description = "AWS region"
}

# Provide access key
variable "access_key" {
    description = "provide the access key for your account"  
}

#provdie access key
variable "secret_key" {
    description = "Provide the secret key for your account"
  }

variable "ami" {
    description = "input AMI information" 
}

variable "instance_type" {
   description = "What is the type required for instance"
}
